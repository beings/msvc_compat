/****************************************************************************
Copyright (c) 2017      dpull.com
http://www.dpull.com
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
#pragma once
// #define USE_SHARED_MUTEX_COMPARE

#include <windows.h>

class shared_mutex_windows
{
private:
	SRWLOCK rwlock;

private:
	shared_mutex_windows(shared_mutex_windows&);
	shared_mutex_windows& operator=(shared_mutex_windows&);

public:
	shared_mutex_windows()
	{
		::InitializeSRWLock(&rwlock);
	}

	~shared_mutex_windows()
	{
	}

	bool try_lock_shared()
	{
		return ::TryAcquireSRWLockShared(&rwlock) == TRUE;
	}

	_Acquires_shared_lock_(rwlock)
	void lock_shared()
	{
		::AcquireSRWLockShared(&rwlock);
	}

	_Releases_shared_lock_(rwlock)
	void unlock_shared()
	{
		::ReleaseSRWLockShared(&rwlock);
	}

	_Releases_exclusive_lock_(rwlock)
	void lock()
	{
		::AcquireSRWLockExclusive(&rwlock);
	}

	bool try_lock()
	{
		return ::TryAcquireSRWLockExclusive(&rwlock) == TRUE;
	}

	_Releases_exclusive_lock_(rwlock)
	void unlock()
	{
		::ReleaseSRWLockExclusive(&rwlock);
	}
};

class shared_mutex_nonlock
{
private:
	shared_mutex_nonlock(shared_mutex_nonlock&);
	shared_mutex_nonlock& operator=(shared_mutex_nonlock&);

public:
	shared_mutex_nonlock()
	{
	}

	bool try_lock_shared()
	{
		return TRUE;
	}

	void lock_shared()
	{
	}

	void unlock_shared()
	{
	}

	void lock()
	{
	}

	bool try_lock()
	{
		return TRUE;
	}

	void unlock()
	{
	}
};

class shared_mutex_compare
{
private:
	CRITICAL_SECTION rwlock;

private:
	shared_mutex_compare(shared_mutex_compare&);
	shared_mutex_compare& operator=(shared_mutex_compare&);

public:
	shared_mutex_compare()
	{
		::InitializeCriticalSection(&rwlock);
	}

	~shared_mutex_compare()
	{
		::DeleteCriticalSection(&rwlock);
	}

	bool try_lock_shared()
	{
		return ::TryEnterCriticalSection(&rwlock) == TRUE;
	}

	void lock_shared()
	{
		::EnterCriticalSection(&rwlock);
	}

	void unlock_shared()
	{
		::LeaveCriticalSection(&rwlock);
	}

	void lock()
	{
		::EnterCriticalSection(&rwlock);
	}

	bool try_lock()
	{
		return ::TryEnterCriticalSection(&rwlock) == TRUE;
	}

	void unlock()
	{
		::LeaveCriticalSection(&rwlock);
	}
};

namespace std {
	typedef shared_mutex_compare shared_mutex;
}