#include "gtest/gtest.h"
#include "shared_mutex.h"
#include <assert.h>
#include <thread>
#include <atomic>

TEST(shared_mutex_windows, lock_shared_test)
{
	shared_mutex_windows mutex;
	std::atomic<int> running_count(0);
	std::atomic<int> max_running_count(0);
	std::thread threads[10];
	threads[0] = std::thread([&](){
		mutex.lock_shared();
		running_count++;
		std::this_thread::sleep_for(std::chrono::microseconds(1000));
		running_count--;
		mutex.unlock_shared();
	});

	for (int i = 1; i < _countof(threads); ++i)
	{
		threads[i] = std::thread([&](){
			mutex.lock_shared();
			EXPECT_TRUE(running_count.load() > 0);

			auto count = ++running_count;
			if (count > max_running_count.load())
				max_running_count.store(count);

			std::this_thread::sleep_for(std::chrono::microseconds(10));
			running_count--;
			mutex.unlock_shared();
		});
	}

	for (int i = 0; i < _countof(threads); ++i)
	{
		threads[i].join();
	}

	ASSERT_EQ(10, max_running_count.load());
}

TEST(shared_mutex_windows, lock_shared_lock_test)
{
	shared_mutex_windows mutex;
	std::atomic<int> running_count(0);
	std::thread threads[10];
	threads[0] = std::thread([&]() {
		mutex.lock_shared();
		running_count++;
		std::this_thread::sleep_for(std::chrono::microseconds(1000));
		running_count--;
		mutex.unlock_shared();
	});

	for (int i = 1; i < _countof(threads); ++i)
	{
		threads[i] = std::thread([&]() {
			mutex.lock();
			EXPECT_EQ(0, running_count.load());

			running_count++;
			std::this_thread::sleep_for(std::chrono::microseconds(10));
			running_count--;
			mutex.unlock();
		});
	}

	for (int i = 0; i < _countof(threads); ++i)
	{
		threads[i].join();
	}
}

TEST(shared_mutex_windows, lock_lock_shared_test)
{
	shared_mutex_windows mutex;
	std::atomic<int> running_count(0);
	std::thread threads[10];
	threads[0] = std::thread([&]() {
		mutex.lock();
		running_count++;
		std::this_thread::sleep_for(std::chrono::microseconds(1000));
		running_count--;
		mutex.unlock();
	});

	for (int i = 1; i < _countof(threads); ++i)
	{
		threads[i] = std::thread([&]() {
			mutex.lock_shared();
			EXPECT_EQ(0, running_count.load());

			std::this_thread::sleep_for(std::chrono::microseconds(10));
			mutex.unlock_shared();
		});
	}

	for (int i = 0; i < _countof(threads); ++i)
	{
		threads[i].join();
	}
}
